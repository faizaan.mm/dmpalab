import itertools
import copy
import csv
import random
import math
records = []
with open('data.csv', 'rt') as f:
    reader = csv.reader(f)
    records=list(reader)
n=len(records[0])
rows = len(records)
for i in range(0,rows):
	for j in range(0,n):
		records[i][j]=float(records[i][j])
k=int(input("enter k "))
c=[]
for i in range(0,k):
	c.append(int(input("enter centre ")))
flag=0
clusters=[]
for i in c:
	s=set()
	s.add(i)
	clusters.append(s)
print('iteration 0')
print(c)
for i in range(0,rows):
	sum=0
	min=999
	min_index=0
	for x in range(0,k):
		sum=0
		for j in range(0,n):
			sum+=((records[i][j]-records[c[x]][j])**2)
		print(sum**0.5)
		if min>sum**0.5:
			min=sum**0.5
			min_index=x
	clusters[min_index].add(i)
print(clusters)
iter = 0
while not flag:
	iter+=1
	print('iteration',iter)
	flag=1
	means=[]
	c=[]
	for i in range(0,k):
		s=clusters[i]
		temp=[]
		for x in range(0,n):
			sum=0
			for j in s:
				sum+=records[j][x]
			sum=sum/len(s)
			temp.append(sum)
		c.append(temp)
	print(c)
	for i in range(0,rows):
		sum=0
		min=999
		min_index=0
		for x in range(0,k):
			sum=0
			for j in range(0,n):
				sum+=((records[i][j]-c[x][j])**2)
			print(sum**0.5)
			if min>sum**0.5:
				min=sum**0.5
				min_index=x
		if i not in clusters[min_index]:
			flag=0
			for j in range(0,k):
				if i in clusters[j]:
					clusters[j].remove(i)
			clusters[min_index].add(i)
	print(clusters)
