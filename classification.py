import csv
from collections import Counter
import math


def info(D):
	
	column = []	
	for i in range(1,len(D)):
		column.append(D[i][-1])
	
	keys = Counter(column).keys()
	values = Counter(column).values()
	entries = len(column)	

	result = 0
	for i in values:
		result -= (i/entries)*math.log2(i/entries)

	return round(result,4)


def I(args):	
	entries = sum(args)
	print(entries)
	print(args)
	result = 0
	for i in args:
		result -= (i/entries)*math.log2(i/entries)
	print(result)
	return round(result,4)


def info_class(D,c):

	cl = D[0][c]	
	column = []	
	output = []
	for i in range(1,len(D)):
		column.append(D[i][c])
		output.append(D[i][-1])
	
	keys = list(Counter(column).keys())
	values = list(Counter(column).values())
	entries = len(column)		
	result = 0
	for i in range(len(keys)):
		temp = []
		print(keys[i])
		for j in range(len(column)):
			if(keys[i]==column[j]):
				temp.append(output[j])		
		new_values = list(Counter(temp).values())		
		result += (values[i]/entries)*I(new_values)

	return round(result,4)




def gain(info1,info2):
	return round(info1-info2,4)




def printMatrix(matrix):
	for i in matrix:
		print(i)


def readinput():
	fileInputStream = open('classification_input.csv','r')
	reader = csv.reader(fileInputStream,delimiter=' ')
	return [row for row in reader if len(row)!=0]


def main():
	D = readinput()
	class_col = int(input("enter class column: \n"))

	for i in range(0,len(D)):
		ele = D[i].pop(class_col)
		D[i].append(ele)

	printMatrix(D)

	print('\n Info(D): ')
	Inf = info(D)
	print(Inf)	

	gains = []
	ratios = []
	print('\nInfo for all classes\n')
	for i in range(len(D[0])-1):
		print('\n',D[0][i])
		I = info_class(D,i)		
		print('Info: ',I)
		G = gain(Inf,I)
		print('Gain: ',G)
		gains.append(G)
		print("Gain Ratio: ", G/I)
		ratios.append(G/I)

	print('\nClass with maximum gain: ', D[0][gains.index(max(gains))])
	print('\nClass with maximum gain ratio: ', D[0][ratios.index(max(ratios))])	

if __name__=='__main__':
	main()