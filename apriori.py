import itertools
import copy
import csv
itemsets = []
min_support=int(input("Enter minimum support"))
with open('test.csv', 'r') as f:
    reader = csv.reader(f)
    itemsets=list(reader)
l =[[]]
count = 0
c1 = []
for i in itemsets:
	for j in i:
		if j not in c1:
			c1.append(j)
freq = {}
for i in itemsets:
	for j in i:
		if j not in freq:
			freq[j]=1
		else:
			freq[j]+=1

for i in freq:
	if freq[i]>=min_support:
		l[0].append(i)
print("1 Frequent Item Set",l[0])
count += 1

c2=list(itertools.combinations(l[0],2))
for i in c2:
	for j in itemsets:
		if i[0] in j and i[1] in j:
			if i not in freq:
				freq[tuple(i)]=1
			else:
				freq[tuple(i)]+=1
temp = []
l.append([])
for i in c2:
	if i in freq and freq[i]>=min_support:
		l[1].append(i)
		for j in i:
			if j not in temp:
				temp.append(j)
print("2 Frequent Item Set",l[1])
count += 1

while temp:
	count+=1
	c=list(itertools.combinations(temp,count))
	flag = 1
	temp1 = copy.deepcopy(c)
	for i in c:
		for j in i:
			if j not in l[0]:
				temp.remove(i)
				flag = 0
				break
		flag = 1
		for j in range(2,len(i)):
			if flag == 0:
				break
			subset = list(itertools.combinations(i,j))
			for k in subset:
				if k not in l[j-1]:
					temp1.remove(i)
					flag=0
					break
	c = temp1
	for i in c:
		for j in itemsets:
			flag=1
			for k in i:
				if k not in j:
					flag=0
					break
			if flag==1:
				if i not in freq:
					freq[i]=1
				else:
					freq[i]+=1
	temp=[]
	l.append([])
	for i in c:
		if freq[i]>=min_support:
			l[count-1].append(i)
			for j in i:
				temp.append(j)
	if temp:
		print("Remaining Frequent item sets",l[count-1])

